# Adaptive-SCM

2D neutron diffusion transient problem solver

Adaptive time stepping stiffness confinement method

Reference:

[1] Wei Xiao, et al., Error Analysis and Adaptive Time Stepping of the Stiffness Confinement Method for Solving Neutron Dynamics Equation, 2021 (in review) 
